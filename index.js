const Joi = require('joi');
const express = require('express');
const app = express();
const config = require('./config.json');
const views = require('./arkeaViews');
const routerArkea = require('./arkea');
const session = require('express-session');
const arkeaConf = config.dev.arkeon;

const port = process.env.PORT || config.dev.defaultPort;

app.use(express.json());
app.use(session({
    secret: 'J9Y89Y3j.JUDO',
    resave: false,
    saveUninitialized: false
  }));
app.use(routerArkea);

app.get('/', (req, res) => {
    req.session.token = undefined;
    res.send(views.indexView(arkeaConf.useCases));
});

app.listen(3000, ()=>{
    console.log("Listening on port "+port+" ...");
});