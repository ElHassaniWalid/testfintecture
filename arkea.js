const config = require('./config.json');
const express = require('express');
const https = require('https');
const qs = require("querystring");

const router = express.Router();
const arkeaConf = config.dev.arkeon;

const views = require('./arkeaViews');

//prepare routes to get information about the accounts
//manage the return status of the request
let getUrlAuthCode = (useCaseIdentity)=>{
    return arkeaConf.authorizeURL +"?response_type=code&client_id="+ arkeaConf.consumerKey +
    "&redirect_uri="+ arkeaConf.callbackURL +
    "&scope=aisp&psu="+useCaseIdentity;
};

let getTokenUrl = ()=>{
    return arkeaConf.requestURL;
}

let setTokenHttpsCall = (params, url)=>{
    var options = {
        "method": "POST",
        "hostname": url,
        "port": 443,
        "path": "/oauth-authorizationcode-psd2/token",
        "headers": {
            "cache-control": "no-cache",
            "content-type": "application/x-www-form-urlencoded"
        }
    };
    let p = new Promise((resolve, reject)=>{

        let req = https.request(options, function (res) {
            var chunks = [];
    
            res.on("data", function (chunk) {
                chunks.push(chunk);
            });
    
            res.on("end", function () {
                if(res.statusCode === 200){
                    var body = Buffer.concat(chunks);
                    resolve(body.toString());
                }else{
                    reject("Error fetching the Token");
                }     
                           
            });
    
        });
        
        req.write( qs.stringify(params) );
        req.end();
    });

    return p;
}
let fetchUser = (token, idUseCase)=>{
    //traitement
}

let getToken = (useCaseIdentity, res, callback) => {
    if(useCaseIdentity === undefined) return "Canno't get AccessToken !";

    // get access code
    let url =  getUrlAuthCode(useCaseIdentity);
    
    return retrievAuthCode(url)
    .then( code => {
        let str = code;
        let i = str.indexOf('=');
        code = str.slice((i+1), str.length);
        return code;
    } )
    .then( result => {
        let params = { 
            client_id: config.dev.arkeon.consumerKey,
            grant_type: 'authorization_code',
            redirect_uri: config.dev.arkeon.callbackURL,
            code: result 
        };
        setTokenHttpsCall(params, getTokenUrl())
        .then((data) => {
            return JSON.parse(data).access_token;
        })
        .then((data) => {
            return callback(data);
        })
        .catch((e)=>{
            console.error(e);
        });
        
    })
    .catch( e => {
        console.error(e);
    });
    
};

router.get("/accounts/:idUseCase", (req, res)=>{

    let id = req.params.idUseCase;
    if(id === undefined) res.status(400);
    
    if(req.session.token !== undefined) getDataAccounts(req.session.token, undefined, res);
    getToken(id, res, function(data) {
        req.session.token = data;
        getDataAccounts(data, undefined, res);
    });

});


router.get("/accounts/:idUseCase/balances", (req, res)=>{
    if(req.session.token === undefined) {res.redirect('/').end(); return;}
    let id = req.params.idUseCase;
    if(id === undefined) res.status(400);

    getDataAccountInformations(req.session.token, id, res, 'balances', views.balancesView);
});

router.get("/accounts/:idUseCase/transactions", (req, res)=>{
    if(req.session.token === undefined) {res.redirect('/'); res.end(); return;}
    let id = req.params.idUseCase;
    if(id === undefined) res.status(400);

    getDataAccountInformations(req.session.token, id, res, 'transactions', views.transactionsView);
});

let retrievAuthCode = (url)=>{
    let p = new Promise((resolve, reject) => {
        https.get(url, (res)=>{
            if(res.statusCode === 302)
                resolve(res.headers.location);
        }).on('error', (e) => {
            reject(e);
        });
    });
    return p;  
}

getRequestOptions = (token, idUseCase, extraPath)=>{
    let apiURL = "/psd2/v1/accounts";
    let path = (idUseCase === undefined) ? apiURL : (apiURL +'/'+ idUseCase);
        path+= (extraPath === undefined) ? '' : '/'+extraPath;

    return {
        "method": "GET",
        "hostname": arkeaConf.requestURL,
        "port": 443,
        "path": path,
        "headers": {
          "accept": "application/hal+json; charset=utf-8",
          "signature": "xxx",
          "x-request-id": "xxx",
          "authorization": "Bearer "+token,
          "cache-control": "no-cache"
        }
      };
}

getDataAccountInformations = (token, idUseCase, response, extraPath, theview)=>{
    
    let options = getRequestOptions(token, idUseCase, extraPath);  
    let req = https.request(options, function (res) {
        let chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          let body = Buffer.concat(chunks);
          let status = res.statusCode;
          if(status === 401) response.status(status).send('Invalide Token refresh the token').end();
          if(status === 200) response.status(status).send(theview(body.toString())).end();
          else response.status(status).send("Code error : "+res.statusCode+". \ncan't fetch data !").end();
        });
    }); 
    req.end(); 
}
 
getDataAccounts = (token, idUseCase, response, extraPath)=>{
    
    let options = getRequestOptions(token, idUseCase, extraPath);  
    let req = https.request(options, function (res) {
        let chunks = [];
      
        res.on("data", function (chunk) {
          chunks.push(chunk);
        });
      
        res.on("end", function () {
          let body = Buffer.concat(chunks);
          let status = res.statusCode;
          
          if(status === 401) {response.status(status).send('Invalide Token refresh the token'); response.end();}
          if(status === 200) {response.status(status).send(views.accountsView(body.toString())); response.end();}
          else {response.status(status).send("Code error : "+res.statusCode+". \ncan't fetch data !"); response.end();}
        });
    });
    req.end();
}
    
module.exports = router;